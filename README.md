Mock Coverage Percentage
---

Use this project to generate coverage percentages and test features that display them.

Use the `COVERAGE` variable when running a pipeline to output a specific value for coverage.
